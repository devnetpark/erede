var sdk = require('../../Index');

var Acquirer = sdk.Acquirer;

var EnvironmentType = sdk.EnvironmentType;

var acquirer = new Acquirer("37502603", "4B56E46EE155CA2A0AC0241C69A1E949F964BB26", EnvironmentType.DEVELOP);

var request = new sdk.TransactionRequest();
request.setCapture(false);
request.setReference("Tales10");
request.setAmount("2055");
request.setCardHolderName("Tales Santos");
request.setCardNumber("5448280000000007");
request.setExpirationMonth("12");
request.setExpirationYear("18");
request.setSecurityCode("235");  
request.setKind(sdk.Kind.Credit);   

// var threeDSecure = new sdk.ThreeDSecureRequest();


// authorizeRequest.setExpirationMonth(12);
// authorizeRequest.setExpirationYear(2017);
// authorizeRequest.setCardNumber(5448280000000007);
// authorizeRequest.setReference('RAR-5535');
// authorizeRequest.setOrigin(1);
// authorizeRequest.setCardHolderName('Portador teste');
// authorizeRequest.setSubscription(0);
// authorizeRequest.setCapture(false);
// authorizeRequest.setInstallments(5);
// threeDSecure.setEci("01");
// threeDSecure.setCavv("02");
// threeDSecure.setXid("02");
// threeDSecure.setEmbedded(false);
// threeDSecure.setOnFailure("http://tales.com.br");
// authorizeRequest.setThreeDSecure(threeDSecure);
// var url = new sdk.UrlRequest();
// url.setKind(sdk.UrlKind.ThreeDSecureFailure);
// url.setUrl("http://tales.com.br");
// url.setKind(sdk.UrlKind.ThreeDSecureFailure);
// url.setUrl("http://www.google.com.br")
// // var urlRequest = new Array(new sdk.UrlRequest());
// // urlRequest.push(url);
// authorizeRequest.setUrls([url]);


acquirer.Authorize(request, function(error, result){
    if(error){
        console.log(error);
    }
    else{
        console.log(result);
    }
});