var RefundRequest = function(amount, urls){
    this.amount        = amount;
    this.urls          = urls;
};

RefundRequest.prototype.getAmount = function(){
    return this.amount;
};

RefundRequest.prototype.setAmount = function(amount){
    this.amount = amount;
};

RefundRequest.prototype.getUrls = function(){
    return this.urls;
}

RefundRequest.prototype.setUrls = function(urls){
    this.urls = urls;
};

RefundRequest.map = function(refundRequest){
    return JSON.stringify(refundRequest);
};

module.exports = RefundRequest;
