var TransactionRequest = function   (
                                    capture, 
                                    kind,
                                    reference,
                                    amount,
                                    installments,
                                    cardHolderName,
                                    cardNumber,
                                    expirationMonth,
                                    expirationYear,
                                    securityCode,
                                    softDescriptor,
                                    subscription,
                                    origin,
                                    distributorAffiliation,
                                    threeDSecure,
                                    iata,
                                    urls){
     
     this.capture                          = capture;
     this.kind                             = kind;
     this.reference                        = reference;
     this.amount                           = amount;
     this.installments                     = installments;
     this.cardHolderName                   = cardHolderName;
     this.cardNumber                       = cardNumber;
     this.expirationMonth                  = expirationMonth;
     this.expirationYear                   = expirationYear;
     this.securityCode                     = securityCode;
     this.softDescriptor                   = softDescriptor;
     this.subscription                     = subscription;
     this.origin                           = origin;
     this.distributorAffiliation           = distributorAffiliation;
     this.threeDSecure                     = threeDSecure;
     this.iata                             = iata;
     this.urls                             = urls;
};

TransactionRequest.prototype.getCapture = function(){
    return this.capture;
};

TransactionRequest.prototype.setCapture = function(capture){
    this.capture = capture;
};

TransactionRequest.prototype.getKind = function(){
    return this.kind;
};

TransactionRequest.prototype.setKind = function(kind){
    this.kind = kind;
};

TransactionRequest.prototype.getReference = function(){
    return this.reference;
};

TransactionRequest.prototype.setReference = function(reference){
    this.reference = reference;
};

TransactionRequest.prototype.getAmount = function(){
    return this.amount;
};

TransactionRequest.prototype.setAmount = function(amount){
    this.amount = amount;
};

TransactionRequest.prototype.getInstallments = function(){
    return this.installments;
};

TransactionRequest.prototype.setInstallments = function(installments){
    this.installments = installments;
};

TransactionRequest.prototype.getCardHolderName = function(){
    return this.cardHolderName;
};

TransactionRequest.prototype.setCardHolderName = function(cardHolderName){
    this.cardHolderName = cardHolderName;
};

TransactionRequest.prototype.getCardNumber = function(){
    return this.cardNumber;
};

TransactionRequest.prototype.setCardNumber = function(cardNumber){
    this.cardNumber = cardNumber;
};

TransactionRequest.prototype.getExpirationMonth = function(){
    return this.expirationMonth;
};

TransactionRequest.prototype.setExpirationMonth = function(expiratinoMonth){
    this.expirationMonth = expiratinoMonth;
};

TransactionRequest.prototype.getExpirationYear = function(){
    return this.expirationYear;
};

TransactionRequest.prototype.setExpirationYear = function(expirationYear){
    this.expirationYear = expirationYear;
};

TransactionRequest.prototype.getSecurityCode = function(){
    return this.securityCode;
};

TransactionRequest.prototype.setSecurityCode = function(securityCode){
    this.securityCode = securityCode;
};

TransactionRequest.prototype.getSoftDescriptor = function(){
    return this.softDescriptor;
};

TransactionRequest.prototype.setSoftDescriptor = function(softDescriptor){
    this.softDescriptor = softDescriptor;
};

TransactionRequest.prototype.getSubscription = function(){
    return this.subscription;
};

TransactionRequest.prototype.setSubscription = function(subscription){
    this.subscription = subscription;
};

TransactionRequest.prototype.getOrigin = function(){
    return this.origin;
};

TransactionRequest.prototype.setOrigin = function(origin){
    this.origin = origin;
};

TransactionRequest.prototype.getDistributorAffiliation = function(){
    return this.distributorAffiliation;
};

TransactionRequest.prototype.setDistributorAffiliation = function(distributorAffiliation){
    this.distributorAffiliation = distributorAffiliation;
};

TransactionRequest.prototype.getThreeDSecure = function(){
    return this.threeDSecure;
};

TransactionRequest.prototype.setThreeDSecure = function(threeDSecure){
    this.threeDSecure = threeDSecure;
};

TransactionRequest.prototype.getIata = function(iata){
    return this.iata;
};

TransactionRequest.prototype.setIata = function(iata){
    this.iata = iata;
};

TransactionRequest.prototype.getUrls = function(){
    return this.urls;
};

TransactionRequest.prototype.setUrls = function(urls){
    this.urls = urls;
};

TransactionRequest.map = function(transactionRequest){
    return JSON.stringify(transactionRequest);
};

module.exports = TransactionRequest;