var responseBase = require('./ResponseBase');
var refundResponse = require('./RefundResponse');

var RefundListResponse = function(returnCode, returnMessage, refunds){
    responseBase.call(this, returnCode, returnMessage);

    this.returnCode = returnCode;
    this.returnMessage = returnMessage;
    this.refunds = refunds;
};

RefundListResponse.prototype = Object.create(responseBase.prototype);

RefundListResponse.prototype.getRefunds = function(){
    return this.refunds;
};

RefundListResponse.prototype.setRefunds = function(refunds){
    this.refunds = refunds;
};

RefundListResponse.map = function(result){
    var response = new RefundListResponse();
    response.setReturnCode(result["returnCode"]);
    response.setReturnMessage(result["returnMessage"]);
    response.setLinks(result["links"]);

    if(result["refunds"] != undefined){
        var refunds = [];

        result["refunds"].forEach(function(el){
            refunds.push(refundResponse.map(el));
        });

        response.setRefunds(refunds);
    }

    return response;
};

module.exports = RefundListResponse;