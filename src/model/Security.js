var Security = function(affiliation, token, environment) {
	this.affiliation 	= affiliation;
	this.token 			= token;
	this.environment 	= environment;
};

Security.prototype.getAffiliation = function() {
	return this.affiliation;
};

Security.prototype.getToken = function() {
	return this.token;
};

Security.prototype.getEnvironment = function() {
	return this.environment;
};

module.exports = Security;