var ResponseBase = function(returnCode, returnMessage, links){
	this.returnCode             = returnCode;
    this.returnMessage          = returnMessage;
    this.links 					= links;
}

ResponseBase.prototype.getReturnCode = function(){
    return this.returnCode;
};

ResponseBase.prototype.setReturnCode = function(returnCode){
    this.returnCode = returnCode;
};

ResponseBase.prototype.getReturnMessage = function(){
    return this.returnMessage;
};

ResponseBase.prototype.setReturnMessage = function(returnMessage){
    this.returnMessage = returnMessage;
};

ResponseBase.prototype.getLinks = function(links){
	return this.links;
}

ResponseBase.prototype.setLinks = function(links){
	var link = require('./Link');

	if(links != undefined){
		var responseLinks = [];

        links.forEach(function(el){
            responseLinks.push(new link(el['method'], el['rel'], el['href']));
        });

        this.links = responseLinks;
    }
}

module.exports = ResponseBase;