var responseBase = require('./ResponseBase');
var threeDSecureResponseModel = require('./ThreeDSecureResponse');

var TransactionResponse = function(
                                    returnCode,
                                    returnMessage,
                                    links,
                                    reference,
                                    tid, 
                                    nsu, 
                                    authorizationCode,
                                    dateTime,
                                    amount,
                                    installments,
                                    cardBin,
                                    last4,
                                    threeDSecure){

    responseBase.call(this, returnCode, returnMessage)

    this.links                  = links;
    this.reference              = reference;
    this.tid                    = tid;
    this.nsu                    = nsu; 
    this.authorizationCode      = authorizationCode;
    this.dateTime               = dateTime;
    this.amount                 = amount;
    this.installments           = installments;
    this.cardBin                = cardBin;
    this.last4                  = last4;
    this.threeDSecure           = threeDSecure;
};

TransactionResponse.prototype = Object.create(responseBase.prototype);

TransactionResponse.prototype.getReference = function(){
    return this.reference;
};

TransactionResponse.prototype.setReference = function(reference){
    this.reference = reference;
};

TransactionResponse.prototype.getTid = function(){
    return this.tid;
};

TransactionResponse.prototype.setTid = function(tid){
    this.tid = tid;
};

TransactionResponse.prototype.getNsu = function(){
    return this.nsu;
};

TransactionResponse.prototype.setNsu = function(nsu){
    this.nsu =nsu;
};

TransactionResponse.prototype.getAuthorizationCode = function(){
    return this.authorizationCode;
};

TransactionResponse.prototype.setAuthorizationCode = function(authorizationCode){
    this.authorizationCode = authorizationCode;
};

TransactionResponse.prototype.getDateTime = function(){
    return this.dateTime;
};

TransactionResponse.prototype.setDateTime = function(dateTime){
    this.dateTime = dateTime;
};

TransactionResponse.prototype.getAmount = function(){
    return this.amount;
};

TransactionResponse.prototype.setAmount = function(amount){
    this.amount = amount;
};

TransactionResponse.prototype.getInstallments = function(){
    return this.installments;
};

TransactionResponse.prototype.setInstallments = function(installments){
    this.installments = installments;
};

TransactionResponse.prototype.getCardBin = function(){
    return this.cardBin;
};

TransactionResponse.prototype.setCardBin = function(cardBin){
    this.cardBin = cardBin;
};

TransactionResponse.prototype.getLast4 = function(){
    return this.last4;
};

TransactionResponse.prototype.setLast4 = function(last4){
    this.last4 = last4;
};

TransactionResponse.prototype.getThreeDSecure = function(){
    return this.threeDSecure;
};

TransactionResponse.prototype.setThreeDSecure = function(threeDSecure){
    this.threeDSecure = threeDSecure;
};

TransactionResponse.map = function(result){
    var response = new TransactionResponse();
    response.setReturnCode(result["returnCode"]);
    response.setReturnMessage(result["returnMessage"]);
    response.setLinks(result['links']);
    
    if(result["threeDSecure"] != undefined){
        var threeDSecureResponse = new threeDSecureResponseModel();
        threeDSecureResponse.setReturnCode(result["threeDSecure"]['returnCode']);
        threeDSecureResponse.setReturnMessage(result["threeDSecure"]['returnMessage']);
        threeDSecureResponse.setEmbedded(result["threeDSecure"]['embedded']);
        threeDSecureResponse.setUrl(result["threeDSecure"]['url']);

        response.setThreeDSecure(threeDSecureResponse);    
    }

    response.setReference(result["reference"]);
    response.setTid(result["tid"]);
    response.setNsu(result["nsu"]); 
    response.setAuthorizationCode(result["authorizationCode"]);
    response.setDateTime(result["dateTime"]);
    response.setAmount(result["amount"]);
    response.setInstallments(result["installments"]);
    response.setCardBin(result["cardBin"]);
    response.setLast4(result["last4"]);
    
    return response;
}

module.exports = TransactionResponse;