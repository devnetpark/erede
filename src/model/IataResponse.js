var IataResponse = function(code, departureTax){
    this.code = code;
    this.departureTax = departureTax;
};

IataResponse.prototype.getCode = function(){
    return this.code;
};

IataResponse.prototype.setCode = function(code){
    this.code = code;
};

IataResponse.prototype.getDepartureTax = function(){
    return this.departureTax;
};

IataResponse.prototype.setDepartureTax = function(departureTax){
    this.departureTax = departureTax;
};

IataResponse.map = function(result){
	var response = new IataResponse();
	response.setCode(result['code']);
	response.setDepartureTax(result['departureTax']);

	return response;
}

module.exports = IataResponse;
