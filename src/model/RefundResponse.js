var responseBase = require('./ResponseBase');
var refundHistoryResponse = require('./RefundHistoryResponse');

var RefundResponse = function(returnCode, returnMessage, refundId, tid, nsu, refundDateTime, cancelId, status, amount, statusHistory ){

    responseBase.call(this, returnCode, returnMessage)

    this.refundId = refundId;
    this.tid = tid;
    this.nsu = nsu;
    this.refundDateTime = refundDateTime;
    this.cancelId = cancelId;
    this.status = status;
    this.amount = amount;
    this.statusHistory = statusHistory;
};

RefundResponse.prototype = Object.create(responseBase.prototype);

RefundResponse.prototype.getRefundId = function(){
    return this.refundId;
};

RefundResponse.prototype.setRefundId = function(refundId){
    this.refundId = refundId;
};

RefundResponse.prototype.getTid = function(){
    return this.tid;
};

RefundResponse.prototype.setTid = function(tid){
    this.tid = tid;
};

RefundResponse.prototype.getNsu = function(){
    return this.nsu;
};

RefundResponse.prototype.setNsu = function(nsu){
    this.nsu = nsu;
};

RefundResponse.prototype.getRefundDateTime = function(){
    return this.refundDateTime;
};

RefundResponse.prototype.setRefundDateTime = function(refundDateTime){
    this.refundDateTime = refundDateTime;
};

RefundResponse.prototype.getCancelId = function(){
    return this.cancelId;
};

RefundResponse.prototype.setCancelId = function(cancelId){
    this.cancelId = cancelId;
};

RefundResponse.prototype.getStatus = function(){
    return this.status;
};

RefundResponse.prototype.setStatus = function(status){
    this.status = status;
};

RefundResponse.prototype.getAmount = function(){
    return this.amount;
};

RefundResponse.prototype.setAmount = function(amount){
    this.amount = amount;
};

RefundResponse.prototype.getStatusHistory = function(){
    return this.statusHistory;
};

RefundResponse.prototype.setStatusHistory = function(statusHistory){
    this.statusHistory = statusHistory;
};

RefundResponse.map = function(result){
    var response = new RefundResponse();
    response.setReturnCode(result["returnCode"]);
    response.setReturnMessage(result["returnMessage"]);
    response.setLinks(result["links"]);
    response.setRefundId(result["refundId"]);
    response.setTid(result["tid"]);
    response.setNsu(result["nsu"]);
    response.setRefundDateTime(result["refundDateTime"]);
    response.setCancelId(result["cancelId"]);
    response.setStatus(result["status"]);
    response.setAmount(result["amount"]);

    if(result["statusHistory"] != undefined){
        var statusHistory = []

        result["statusHistory"].forEach(function(el){
            statusHistory.push(refundHistoryResponse.map(el));
        })

        response.setStatusHistory(statusHistory);
    }
    
    return response;
}

module.exports = RefundResponse;