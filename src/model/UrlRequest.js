var UrlRequest = function(kind, url){
    this.kind   = kind;
    this.url    = url;
};

UrlRequest.prototype.getKind = function(){
    return this.urlKind;
};

UrlRequest.prototype.setKind = function(kind){
    this.kind = kind;
};

UrlRequest.prototype.getUrl = function(){
    return this.url;
};

UrlRequest.prototype.setUrl = function(url){
    this.url = url;
};

module.exports = UrlRequest;