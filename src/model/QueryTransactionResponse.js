var responseBase = require('./ResponseBase');
var authorizationResponse = require('./AuthorizationResponse');
var captureResponse = require('./CaptureResponse');
var iataResponse = require('./IataResponse');
var refundResponse = require('./RefundResponse');
var threeDSecureResponse = require('./ThreeDSecureResponse');

var QueryTransactionResponse = function(returnCode, returnMessage, requestDateTime, authorization, capture, iata, threeDSecure, refunds){
    
    responseBase.call(this, returnCode, returnMessage);

    this.requestDateTime =  requestDateTime;
    this.authorization =  authorization;
    this.capture = capture;
    this.iata = iata;
    this.threeDSecure = threeDSecure;
    this.refunds = refunds;
};

QueryTransactionResponse.prototype = Object.create(responseBase.prototype);

QueryTransactionResponse.prototype.getRequestDateTime = function(){
    return this.requestDateTime;
};

QueryTransactionResponse.prototype.setRequestDateTime = function(requestDateTime){
    this.requestDateTime = requestDateTime;
};

QueryTransactionResponse.prototype.getAuthorization = function(){
    return this.authorization;
};

QueryTransactionResponse.prototype.setAuthorization = function(authorization){
    this.authorization = authorization;
};

QueryTransactionResponse.prototype.getCapture = function(){
    return this.capture;
};

QueryTransactionResponse.prototype.setCapture = function(capture){
    this.capture = capture;
};

QueryTransactionResponse.prototype.getIata = function(){
    return this.iata;
};

QueryTransactionResponse.prototype.setIata = function(iata){
    this.iata = iata;
};

QueryTransactionResponse.prototype.getThreeDSecure = function(){
    return this.threeDSecure;
};

QueryTransactionResponse.prototype.setThreeDSecure = function(threeDSecure){
    this.threeDSecure = threeDSecure;
};

QueryTransactionResponse.prototype.getRefunds = function(){
    return this.refunds;
};

QueryTransactionResponse.prototype.setRefunds = function(refunds){
    this.refunds = refunds;
};

QueryTransactionResponse.map = function(result){
    var response = new QueryTransactionResponse();
    response.setReturnCode(result["returnCode"]);
    response.setReturnMessage(result["returnMessage"]);
    response.setLinks(result["links"]);
    response.setRequestDateTime(result["requestDateTime"]);

    if(result["authorization"] != undefined){
        response.setAuthorization(authorizationResponse.map(result["authorization"]));
    }

    if(result["capture"] != undefined){
        response.setCapture(captureResponse.map(result["capture"]));
    }

    if(result["iata"] != undefined){
        response.setIata(iataResponse.map(result["iata"]));
    }

    if(result["threeDSecure"] != undefined){
        response.setThreeDSecure(threeDSecureResponse.map(result["threeDSecure"]));
    }
    
    if(result["refunds"] != undefined){
        var refunds = []
        
        result["refunds"].forEach(function(el){
            refunds.push(refundResponse.map(el));
        })

        response.setRefunds(refunds);
    }
    
    return response;
}

module.exports = QueryTransactionResponse;
