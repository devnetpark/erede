var responseBase = require('./ResponseBase');

var AuthorizationResponse = function(){

};

AuthorizationResponse.prototype = Object.create(responseBase.prototype);

AuthorizationResponse.prototype.getDateTime = function(){
    return this.dateTime;
};

AuthorizationResponse.prototype.setDateTime = function(dateTime){
    this.dateTime = dateTime;
};

AuthorizationResponse.prototype.getAffiliation = function(){
    return this.affiliation;
};

AuthorizationResponse.prototype.setAffiliation = function(affiliation){
    this.affiliation = affiliation;
};

AuthorizationResponse.prototype.getStatus = function(){
    return this.status;
};

AuthorizationResponse.prototype.setStatus = function(status){
    this.status = status;
};

AuthorizationResponse.prototype.getReference = function(){
    return this.reference;
};

AuthorizationResponse.prototype.setReference = function(reference){
    this.reference = reference;
};

AuthorizationResponse.prototype.getTid = function(){
    return this.tid;
};

AuthorizationResponse.prototype.setTid = function(tid){
    this.tid = tid;
};

AuthorizationResponse.prototype.getNsu = function(){
    return this.nsu;
};

AuthorizationResponse.prototype.setNsu = function(nsu){
    this.nsu = nsu;
};

AuthorizationResponse.prototype.getAuthorizationCode = function(){
    return this.authorizationCode;
};

AuthorizationResponse.prototype.setAuthorizationCode = function(authorizationCode){
    this.authorizationCode = authorizationCode;
};

AuthorizationResponse.prototype.getKind = function(){
    return this.kind;
};

AuthorizationResponse.prototype.setKind = function(kind){
    this.kind = kind;
};

AuthorizationResponse.prototype.getAmount = function(){
    return this.amount;
};

AuthorizationResponse.prototype.setAmount = function(amount){
    this.amount = amount;
};

AuthorizationResponse.prototype.getInstallments = function(){
    return this.installments;
};

AuthorizationResponse.prototype.setInstallments = function(installments){
    this.installments = installments;
};

AuthorizationResponse.prototype.getCardHolderName = function(){
    return this.cardHolderName;
};

AuthorizationResponse.prototype.setCardHolderName = function(cardHolderName){
    this.cardHolderName = cardHolderName;
};

AuthorizationResponse.prototype.getCardBin = function(){
    return this.cardBin;
};

AuthorizationResponse.prototype.setCardBin = function(cardBin){
    this.cardBin = cardBin;
};

AuthorizationResponse.prototype.getLast4 = function(){
    return this.last4;
};

AuthorizationResponse.prototype.setLast4 = function(last4){
    this.last4 = last4;
};

AuthorizationResponse.prototype.getSoftDescriptor = function(){
    return this.softDescriptor;
};

AuthorizationResponse.prototype.setSoftDescriptor = function(softDescriptor){
    this.softDescriptor = softDescriptor;
};

AuthorizationResponse.prototype.getOrigin = function(){
    return this.origin;
};

AuthorizationResponse.prototype.setOrigin = function(origin){
    this.origin = origin;
};

AuthorizationResponse.prototype.getSubscription = function(){
    return this.subscription;
};

AuthorizationResponse.prototype.setSubscription = function(subscription){
    this.subscription = subscription;
};

AuthorizationResponse.prototype.getDistributorAffiliation = function(){
    return this.distributorAffiliation;
};

AuthorizationResponse.prototype.setDistributorAffiliation = function(distributorAffiliation){
    this.distributorAffiliation = distributorAffiliation;
};

AuthorizationResponse.map = function(result){
    var response = new AuthorizationResponse();
    response.setReturnCode(result["returnCode"]);
    response.setReturnMessage(result["returnMessage"]);
    response.setDateTime(result['dateTime']);
    response.setAffiliation(result['affiliation']);
    response.setStatus(result['status']);
    response.setReference(result['reference']);
    response.setTid(result['tid']);
    response.setNsu(result['nsu']);
    response.setAuthorizationCode(result['authorizationCode']);
    response.setKind(result['kind']);
    response.setAmount(result['amount']);
    response.setInstallments(result['installments']);
    response.setCardHolderName(result['cardHolderName']);    
    response.setCardBin(result['cardBin']);
    response.setLast4(result['last4']);
    response.setSoftDescriptor(result['softDescriptor']);
    response.setOrigin(result['origin']);    
    response.setSubscription(result['subscription']);
    response.setDistributorAffiliation(result['distributorAffiliation']);

    return response;
}

module.exports = AuthorizationResponse;