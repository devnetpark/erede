var IataRequest = function(code, departureTax){
    this.code          = code;
    this.departureTax  = departureTax;
};

IataRequest.prototype.getCode = function(){
    return this.code;
};

IataRequest.prototype.setCode = function(code){
    this.code = code;
};

IataRequest.prototype.getDepartureTax = function(){
    return this.departureTax;
};

IataRequest.prototype.setDepartureTax = function(departureTax){
    this.departureTax = departureTax;
};

module.exports = IataRequest;
