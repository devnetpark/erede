var responseBase = require('./ResponseBase');

var ThreeDSecureResponse = function(){

};

ThreeDSecureResponse.prototype = Object.create(responseBase.prototype);

ThreeDSecureResponse.prototype.getCavv = function(){
    return this.cavv;
};

ThreeDSecureResponse.prototype.setCavv = function(cavv){
    this.cavv = cavv;
};

ThreeDSecureResponse.prototype.getEci = function(){
    return this.eci;
};

ThreeDSecureResponse.prototype.setEci = function(eci){
    this.eci = eci;
};

ThreeDSecureResponse.prototype.getXid = function(){
    return this.xid;
};

ThreeDSecureResponse.prototype.setXid = function(xid){
    this.xid = xid;
};

ThreeDSecureResponse.prototype.getEmbedded = function(){
    return this.embedded;
};

ThreeDSecureResponse.prototype.setEmbedded = function(embedded){
    this.embedded = embedded;
};

ThreeDSecureResponse.prototype.getUrl = function(){
    return this.url;
};

ThreeDSecureResponse.prototype.setUrl = function(url){
    this.url = url;
};

ThreeDSecureResponse.map = function(result){
    var response = new ThreeDSecureResponse();
    response.setReturnCode(result['returnCode']);
    response.setReturnMessage(result['returnMessage']);
    response.setCavv(result['cavv']);
    response.setEci(result['eci']);
    response.setXid(result['xid']);
    response.setEmbedded(result['embedded']);

    return response;
}

module.exports = ThreeDSecureResponse;