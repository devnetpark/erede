var RefundHistoryResponse = function(){

};

RefundHistoryResponse.prototype.getStatus = function(){
    return this.status;
};

RefundHistoryResponse.prototype.setStatus = function(status){
    this.status = status;
};

RefundHistoryResponse.prototype.getDateTime = function(){
    return this.dateTime;
};

RefundHistoryResponse.prototype.setDateTime = function(dateTime){
    this.dateTime = dateTime;
};

RefundHistoryResponse.map = function(result){
	var response = new RefundHistoryResponse();
	response.setStatus(result['status']);
	response.setDateTime(result['dateTime']);

	return response;
}

module.exports = RefundHistoryResponse;

