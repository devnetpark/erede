var CaptureResponse = function(){

};

CaptureResponse.prototype.getDateTime = function(){
    return this.dateTime;
};

CaptureResponse.prototype.setDateTime = function(dateTime){
    this.dateTime = dateTime;
};

CaptureResponse.prototype.getNsu = function(){
    return this.nsu;
};

CaptureResponse.prototype.setNsu = function(nsu){
    this.nsu = nsu;
};

CaptureResponse.prototype.getAmount = function(){
    return this.amount;
};

CaptureResponse.prototype.setAmount = function(amount){
    this.amount = amount;
};

CaptureResponse.map = function(result){
	var response = new CaptureResponse();
	response.setDateTime(result['dateTime']);
	response.setNsu(result['nsu']);
	response.setAmount(result['amount']);

	return response;
}

module.exports = CaptureResponse;