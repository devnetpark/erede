module.exports = {
     /// <summary>
    /// None.
    /// </summary>
    None : "None",

    /// <summary>
    /// ThreeDsSuccess.
    /// </summary>
    ThreeDSecureSuccess : "ThreeDSecureSuccess",

    /// <summary>
    /// ThreeDsFailed.
    /// </summary>
    ThreeDSecureFailure : "ThreeDSecureFailure",

    /// <summary>
    /// RefundCallback.
    /// </summary>
    Callback : "Callback"
};