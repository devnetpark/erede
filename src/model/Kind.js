module.exports = {
    /// <summary>
    /// Unknown
    /// </summary>
    Unknown : "Unknown",

    /// <summary>
    /// Credit.
    /// </summary>
    Credit : "Credit",

    /// <summary>
    /// Debit.
    /// </summary>
    Debit : "Debit"
};