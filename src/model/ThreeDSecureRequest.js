var ThreeDSecureRequest = function(eci, cavv, xid, embedded, onFailure, userAgent){
    this.eci           = eci;
    this.cavv          = cavv;
    this.xid           = xid;
    this.embedded      = embedded;
    this.onFailure     = onFailure;
    this.userAgent     = userAgent;
};

ThreeDSecureRequest.prototype.getEci = function(){
    this.eci;
};

ThreeDSecureRequest.prototype.setEci = function(eci){
    return this.eci = eci;
};

ThreeDSecureRequest.prototype.getCavv = function(){
    return this.cavv;
};

ThreeDSecureRequest.prototype.setCavv = function(cavv){
    this.cavv = cavv;
};

ThreeDSecureRequest.prototype.getXid = function(){
    return this.xid;
};

ThreeDSecureRequest.prototype.setXid = function(xid){
    this.xid = xid;
};

ThreeDSecureRequest.prototype.getEmbedded = function(){
    return this.embedded;
};

ThreeDSecureRequest.prototype.setEmbedded = function(embedded){
    this.embedded = embedded;
};

ThreeDSecureRequest.prototype.getOnFailure = function(){
    return this.onFailure;
};

ThreeDSecureRequest.prototype.setOnFailure = function(onFailure){
    this.onFailure = onFailure;
};

ThreeDSecureRequest.prototype.getUserAgent = function(){
    return this.userAgent;
};

ThreeDSecureRequest.prototype.setUserAgent = function(userAgent){
    this.userAgent = userAgent;
};

module.exports = ThreeDSecureRequest;