var Link = function(method, rel, href){
    this.method = method;
    this.rel = rel;
    this.href = href;
};

Link.prototype.getMethod = function(){
    return this.method;
};

Link.prototype.getRel = function(){
    return this.rel;
};

Link.prototype.getHref = function(){
    return this.href;
};

module.exports = Link;