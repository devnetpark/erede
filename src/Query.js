var Security = require('./model/Security');
var EnvironmentType = require('./model/EnvironmentType');
var Client = require('node-rest-client').Client;
var QueryTransactionResponse = require('./model/QueryTransactionResponse');
var RefundListResponse = require ('./model/RefundListResponse');
var RefundResponse = require ('./model/RefundResponse');

/**
 * Constructor for Query.
 */
var Query = function(affiliation, token, environment){
    this._security = new Security(affiliation, token, environment);
    this._options_auth = { "user": this._security.affiliation, "password": this._security.token };
    this._header = {
        'Content-Type' : 'application/json',
    };
};

/**
 * Calls Query Transaction by Tid.
 * @param Tid
 * @return QueryTransactionResponse
 */
Query.prototype.QueryTransactionByTid = function(tid, callback){
    var client = new Client(this._options_auth);
    var post_options= {
        header : this._header
    };
    client.get(GetQueryTransaction(this._security.environment, tid, ""), post_options, function(response, result){
        callback(null, QueryTransactionResponse.map(response));
    }).on('error', function(err){
        callback(getErrorResponse(err, new QueryTransactionResponse()));
    });
};

/**
 * Calls Query Transaction by Reference.
 * @param Reference.
 * @return QueryTransactionResponse
 */
Query.prototype.QueryTransactionByReference = function(reference, callback){
    var client = new Client(this._options_auth);
    var post_options = {
        header : this._header
    };
    client.get(GetQueryTransaction(this._security.environment, "", reference), post_options, function(response, result){
        callback(null, QueryTransactionResponse.map(response));
    }).on('error', function(err){
        callback(getErrorResponse(err, new QueryTransactionResponse()));
    });
};

/**
 * Calls Query Transaction.
 * @param Tid
 * @param Reference.
 * @return QueryTransactionResponse
 */
Query.prototype.QueryTransaction = function(tid, reference, callback){
    var client = new Client(this._options_auth);
    var post_options = {
        header : this._header
    };
    client.get(GetQueryTransaction(this._security.environment, tid, reference), post_options, function(response, result){
        callback(null, QueryTransactionResponse.map(response));
    }).on('error', function(err){
        callback(getErrorResponse(err, new QueryTransactionResponse()));
    });
};

/**
 * Calls Query Refunds.
 * @param Tid.
 * @return RefundListResponse
 */
Query.prototype.QueryRefunds = function(tid, callback){
    var client = new Client(this._options_auth);
    var post_options = {
        header : this._header
    };
    client.get(GetQueryRefundsUrl(this._security.environment, tid), post_options, function(response, result){
        callback(null, RefundListResponse.map(response));
    }).on('error', function(err){
        callback(getErrorResponse(err, new RefundListResponse()));
    });
};

/**
 * Calls Query Refund.
 * @param Tid.
 * @param Refund Id.
 * @return RefundResponse
 */
Query.prototype.QueryRefund = function(tid, refundId, callback){
    var client = new Client(this._options_auth);
    var post_options = {
        header : this._header
    };
    client.get(GetQueryRefundUrl(this._security.environment, tid, refundId), post_options, function(response, result){
        callback(null, RefundResponse.map(response));
    }).on('error', function(err){
        callback(getErrorResponse(err, new RefundResponse()));
    });
};

/**
 * Create url based on environment.
 * @param EnvironmentType .
 * @returns Url PRODUCTION/HOMOLOG/DEVELOPMENT.
 */
var RestClientProxy = function(environment) {
	switch(environment) {
	case EnvironmentType.HOMOLOG:
		this._endpoint = 'https://api.userede.com.br/desenvolvedores/v1/';
		break;
	case EnvironmentType.PRODUCTION:
		this._endpoint = 'https://api.userede.com.br/v1/';
		break;
	case EnvironmentType.DEVELOP:
		this._endpoint = 'http://localhost:56513';
		break;
	}
	return this._endpoint;
};



/**
 * Gets the GetQueryTransaction url.
 * @param EnvironmentType.
 * @param Tid.
 * @param Reference.
 * @returns Url QueryTransaction
 */
var GetQueryTransaction = function(environment, tid, reference){
    var url = RestClientProxy(environment) + "transactions/?tid=" + tid + "&reference=" + reference;
    return url;
};

/**
 * Gets the QueryRefunds url.
 * @param EnvironmentType.
 * @param Tid.
 * @returns Url QueryRefunds.
 */
var GetQueryRefundsUrl = function(environment, tid){
    var url = RestClientProxy(environment) + "transactions/" + tid + "/refunds";
    return url;
};

/**
 * Gets the QueryRefund url.
 * @param EnvironmentType.
 * @param Tid.
 * @param refund Id.
 * @returns Url QueryRefund.
 * 
 */
var GetQueryRefundUrl = function(environment, tid, refundId){
    var url = RestClientProxy(environment) + "transactions/" + tid + "/refunds/" + refundId;
    return url;
};

/**
 * Get the error message and code.
 * @param Error.
 * @param Response.
 * @returns Return Code and Return message of an error ocurred.
 */
var getErrorResponse = function(error, response){
    response.returnCode = "370";
	response.returnMessage = error.message;
	return response;
}
module.exports = Query;