var auth = require('basic-auth')
var Security = require('./model/Security');
var EnvironmentType = require('./model/EnvironmentType');
var Client = require('node-rest-client').Client;
var TransactionRequest = require('./model/TransactionRequest');
var TransactionResponse = require('./model/TransactionResponse');
var RefundRequest = require('./model/RefundRequest');
var RefundResponse = require('./model/RefundResponse');

/**
 * Constructor for Acquirer
 */
var Acquirer = function(affiliation, token, environment){
    this._security = new Security(affiliation, token, environment);
	this._options_auth = { "user": this._security.affiliation, "password": this._security.token };
    this._header = {
        'Content-Type' : 'application/json'
    };
};

/**
 * Calls authorize method.
 * @param: request (TransactionRequest).
 * @return callback (Transaction Response).
 */
Acquirer.prototype.Authorize = function(request, callback){
	var client = new Client(this._options_auth);
	var post_options = {
		data : TransactionRequest.map(request),
		headers : this._header
	}
	client.post(GetAuthorizeUrl(this._security.environment), post_options, function(response, result){
		callback(null, TransactionResponse.map(response));
	 }).on('error', function(err){
        callback(getErrorResponse(err, new TransactionResponse()));
    });
};

/**
 * Calls refund method.
 * @param RefundRequest.
 * @return callback (Refund Response).
 */
Acquirer.prototype.Refund = function(tid, request, callback){
	var client = new Client(this._options_auth);
	var post_options = {
		data : RefundRequest.map(request),
		headers: this._header
	};
	client.post(GetRefundUrl(this._security.environment, tid), post_options, function(response, result){
		callback(null, RefundResponse.map(response));
	 }).on('error', function(err){
        callback(getErrorResponse(err, new RefundResponse()));
    });
};

/**
 * Call capture method.
 * @param TransactionRequest.
 * @return TransactionResponse.
 */
Acquirer.prototype.Capture = function(tid, request, callback){
	var client = new Client(this._options_auth);
	var post_options = {
		data : TransactionRequest.map(request),
		headers : this._header
	};
	client.put(GetCaptureUrl(this._security.environment, tid), post_options, function(response, result){
		callback(null, TransactionResponse.map(response));
	}).on('error', function(err){
        callback(getErrorResponse(err, new TransactionResponse()));
    });
};

/**
 * Create url based on environment.
 * @param EnvironmentType .
 * @returns Url PRODUCTION/HOMOLOG/DEVELOPMENT.
 */
var RestClientProxy = function(environment) {
	switch(environment) {
	case EnvironmentType.HOMOLOG:
		this._endpoint = 'https://api.userede.com.br/desenvolvedores/v1/';
		break;
	case EnvironmentType.PRODUCTION:
		this._endpoint = 'https://api.userede.com.br/v1/';
		break;
	case EnvironmentType.DEVELOP:
		this._endpoint = 'http://localhost:56513';
		break;
	}
	return this._endpoint;
};

/**
 * GetAuthorizeUrl.
 * @param EnvironmentType.
 * @returns Url.
 */
var GetAuthorizeUrl = function(environment){
	var url =  RestClientProxy(environment) + "/transactions";
	return url;
};

/**
 * GetRefundUrl.
 * 
 */
var GetRefundUrl = function(environment, tid){
	var url = RestClientProxy(environment) + "/transactions/" + tid + "/refunds";
	return url;
};

/**
 * GetCaptureUrl.
 * @param EnvironmentType.
 * @param Tid.
 * @returns Return url to make the request.
 */
var GetCaptureUrl = function(environment, tid){
	var url = RestClientProxy(environment) + "/transactions/" + tid;
	return url;
};

/**
 * Get the error message and code.
 * @param Error.
 * @param Response.
 * @returns Return Code and Return message of an error ocurred.
 */
var getErrorResponse = function (error, response) {
	response.returnCode = "370";
	response.returnMessage = error.message;

	return response;
};

module.exports = Acquirer;